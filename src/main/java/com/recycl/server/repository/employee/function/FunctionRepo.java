package com.recycl.server.repository.employee.function;

import com.recycl.server.domain.employee.function.Function;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FunctionRepo extends JpaRepository<Function, Long> {
}
