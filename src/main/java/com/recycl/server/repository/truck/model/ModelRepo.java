package com.recycl.server.repository.truck.model;

import com.recycl.server.domain.truck.model.Model;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ModelRepo extends JpaRepository<Model, Long> {
}
