package com.recycl.server.repository.truck;

import com.recycl.server.domain.truck.Truck;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TruckRepo extends JpaRepository<Truck, Long> {
}
