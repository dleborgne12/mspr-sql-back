package com.recycl.server.api.truck;

import com.recycl.server.domain.truck.Truck;
import com.recycl.server.service.truck.TruckService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/truck")
@AllArgsConstructor
public class TruckQuery {

    private final TruckService truckService;

    @GetMapping("/all")
    public ResponseEntity<List<Truck>> getAllTruck() {
        List<Truck> trucks = truckService.findAll();
        return new ResponseEntity<>(trucks, HttpStatus.OK);
    }
}
