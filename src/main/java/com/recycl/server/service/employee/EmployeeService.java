package com.recycl.server.service.employee;

import com.recycl.server.domain.employee.Employee;
import com.recycl.server.repository.employee.EmployeeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

  private final EmployeeRepo employeeRepo;

  @Autowired
  public EmployeeService(EmployeeRepo employeeRepo) {
    this.employeeRepo = employeeRepo;
  }

  public List<Employee> findAll() {
    return this.employeeRepo.findAll();
  }
}
