package com.recycl.server.service.employee.function;

import com.recycl.server.domain.employee.function.Function;
import com.recycl.server.repository.employee.function.FunctionRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class FunctionService {

    private final FunctionRepo functionRepo;

    @Autowired
    public FunctionService(FunctionRepo functionRepo) {
        this.functionRepo = functionRepo;
    }

    public Optional<Function> findFunction(Long id) {
        return this.functionRepo.findById(id);
    }
}
