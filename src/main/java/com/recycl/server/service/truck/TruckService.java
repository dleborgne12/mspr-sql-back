package com.recycl.server.service.truck;

import com.recycl.server.domain.truck.Truck;
import com.recycl.server.repository.truck.TruckRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TruckService {

    private final TruckRepo truckRepo;

    @Autowired
    public TruckService(TruckRepo truckRepo) {
        this.truckRepo = truckRepo;
    }

    public List<Truck> findAll() {
        return this.truckRepo.findAll();
    }
}
