package com.recycl.server.domain.employee;

import com.recycl.server.domain.employee.function.Function;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private String name;
    private String firstname;
    private LocalDate birthday;
    private LocalDate hiredate;
    private Long salary;

    @ManyToOne
    @JoinColumn(name = "function_id")
    private Function function;
}
