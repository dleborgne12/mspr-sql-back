FROM openjdk:16-alpine3.13
ARG JAR_FILE=target/back-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/app.jar"]